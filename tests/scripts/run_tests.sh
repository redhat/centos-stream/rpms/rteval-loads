#!/usr/bin/bash

# make sure we have rteval installed
if rpm -q --quiet rteval-loads; then
    :
else
    sudo dnf install -y rteval-loads
    if [[ $? != 0 ]]; then
	echo "install of rteval failed!"
	exit 1
    fi
fi

# check that the tarball is in place
if [[ ! -f /usr/share/rteval/loadsource/linux-6.12-rc4.tar.gz ]]; then
    echo "No load tarball found!"
    exit 3
fi

exit 0
